let a = Number(prompt('Введіть перше число'));
let b = Number(prompt('Введіть друге число'));
let mathAction = prompt('Сюди може бути введено', ' + , - , * , / ');

function runCalculate(a, b, mathAction) {
    switch (mathAction) {
        case "+":
            return a + b;
        case "-":
            return a - b;
        case "*":
            return a * b;
        case "/":
            return a / b;
        default:
            break;    
    }
}

console.log(runCalculate(+a, +b, mathAction));
